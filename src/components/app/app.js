import React, { Component } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ErrorBoundry from "../error-boundry";

import './app.css';
import Row from '../row';
import ItemDetails, { Record } from '../item-details/item-details';
import SwapiService from '../../services/swapi-service';

export default class App extends Component {

  swapiService = new SwapiService()
  state = {
    showRandomPlanet: true
  };

  toggleRandomPlanet = () => {
    this.setState((state) => {
      return {
        showRandomPlanet: !state.showRandomPlanet
      }
    });
  };

  render() {

    const planet = this.state.showRandomPlanet ?
      <RandomPlanet/> :
      null;

    const {getPerson,  getStarship, getPersonImage, getStarshipImage} = this.swapiService;
    const personDetails = (
      <ItemDetails itemId={11} 
                    getData={getPerson}
                    getImageUrl={getPersonImage}>
        <Record field="gender" label="Gender" />
        <Record field="eyeColor" label="Eye color" />
      </ItemDetails>
    )
    const starshipDetails = (
      <ItemDetails itemId={5} 
                  getData={getStarship}
                  getImageUrl={getStarshipImage}>
      
        <Record field="model" label="Model" />
        <Record field="costInCredits" label="Cost in credits"/>
        <Record field="length" label="Length"/>

      </ItemDetails>
    )
    
    
    return (
      <ErrorBoundry>
        <div className="stardb-app">
          <Header />
          { planet }

         <Row left={personDetails} right={starshipDetails}/>

        </div>
      </ErrorBoundry>
    );
  }
}
